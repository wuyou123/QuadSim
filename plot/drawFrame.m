% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [frame] = drawFrame(parent,pos,long)
%   _____________________
% 
%   Generate the 3D frame axis handlers with rgb.
%
%   [hx,posx,hy,posy,hz,posz] = drawFrame(parent,pos)
%
%   Inputs:
%       - parent:   Axes handler where the quadrotor will be plotted. 
%       - pos:      Position of the frame origin in 3D.
%
%   Ouputs:
%       - hx:       X axis object handler.
%       - posx:     Positions of the X axis vertices.
%       - hy:       Y axis object handler.
%       - posy:     Positions of the Y axis vertices.
%       - hz:       Z axis object handler.
%       - posz:     Positions of the Z axis vertices.
%
%   _____________________

posx=[pos(1,1) pos(1,1)+long;
      pos(2,1) pos(2,1);
      pos(3,1) pos(3,1)];

hx=drawLine(parent,posx,'r');  
  
posy=[pos(1,1) pos(1,1);
      pos(2,1) pos(2,1)+long;
      pos(3,1) pos(3,1)];
      
hy=drawLine(parent,posy,'g');

posz=[pos(1,1) pos(1,1);
      pos(2,1) pos(2,1);
      pos(3,1) pos(3,1)+long];
hz=drawLine(parent,posz,'b');
  
frame=[hx,hy,hz];
hold on
return