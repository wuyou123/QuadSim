% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [] = updateAxis(axishdl,state)
%   _____________________
% 
%   Update Current Figure Axis
%
%   [] = updateAxis(axishdl,state)
%
%   Inputs:
%       - axishdl:   Current Axes handler.
%       - state:     New State.
%   _____________________

    offset = 0.5;
    
    xlim=get(axishdl,'XLim');
    minx = min(state(1)-offset,xlim(1));
    maxx = max(state(1)+offset,xlim(2));
      
    ylim=get(axishdl,'YLim');
    miny = min(state(2)-offset,ylim(1));
    maxy = max(state(2)+offset,ylim(2));
      
    zlim=get(axishdl,'ZLim');
    minz = 0.0;
    maxz = max(state(3)+offset,zlim(2));

    axis([minx maxx miny maxy minz maxz]);  

return