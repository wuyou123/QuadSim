% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [] = updateWP(hndl,color,visible)
%   _____________________
% 
%   Update Waypoint Handler
%
%   [] = updateWP(hndl,color,visible)
%
%   Inputs:
%       - hndl:      Waypoint handler to be updated.
%       - color:     New Waypoint color.
%       - visible:   Enables/disables handle visibility.
%   _____________________

if strcmp(visible,'on')
    
    % To allow overlapping
    x = get(hndl,'XData')';
    y = get(hndl,'YData')';
    z = get(hndl,'ZData')';
    xoff = 0.01*(x-repmat(mean(x),size(x,1),1));
    yoff = 0.01*(y-repmat(mean(y),size(y,1),1));
    zoff = 0.01*(z-repmat(mean(z),size(z,1),1));
    set(hndl,'XData',x-xoff,'YData',y-yoff,'ZData',z-zoff); 
   
    set(hndl,'FaceColor',color);
else
    set(hndl,'Visible',visible);
end

return