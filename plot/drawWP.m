% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [hndl] = drawWP(parent,wp_info)
%   _____________________
% 
%   Generate a WayPoint (Sphere) handler.
%
%   [hndl] = drawWP(parent,wp_info)
%
%   Inputs:
%       - parent:       Axes handler where the quadrotor will be plotted. 
%       - wp_info:      Waypoint info: [x,y,z,tolerance].
% 
%   Ouputs:
%       - hndl:         Waypoint handler.
%   _____________________

[x,y,z] = sphere(parent);
hndl = surf(wp_info(4)*x+wp_info(1), wp_info(4)*y+wp_info(2), wp_info(4)*z+wp_info(3));

set(hndl,'FaceColor',[1 1 0],'FaceAlpha',0.5,'EdgeColor','none');

hold on
return