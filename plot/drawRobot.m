% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [hquad] = drawRobot(parent,pos,plot_params,visible,name)
%   _____________________
% 
%   Generate a Quadrotor handler.
%
%   [hquad] = drawRobot(parent,pos,plot_params,visible,name)
%
%   Inputs:
%       - parent:       Axes handler where the quadrotor will be plotted. 
%       - pos:          Position of the quadrotor in 3D.
%       - plot_params:  Parameters.
%       - visible:      Enable or diable handle visiblity.
%       - name:         Quadrotor name (optional).
%
%   Ouputs:
%       - hquad:           Quadrotor handler.
%   _____________________

r_quad = plot_params.robot.quad.params.size_m2m/2;
color = plot_params.robot.quad.params.color;
frame_len = plot_params.frame_length;
crad = r_quad/1.3;

%Rotors
rot1 = drawCylinder(parent,pos+[r_quad;r_quad;0],pos+[r_quad;r_quad;0.01],crad,20,[1 0 0],1,0,0.1);
rot2 = drawCylinder(parent,pos+[r_quad;-r_quad;0],pos+[r_quad;-r_quad;0.01],crad,20,[1 0 0],1,0,0.1);
rot3 = drawCylinder(parent,pos+[-r_quad;r_quad;0],pos+[-r_quad;r_quad;0.01],crad,20,color,1,0,0.1);
rot4 = drawCylinder(parent,pos+[-r_quad;-r_quad;0],pos+[-r_quad;-r_quad;0.01],crad,20,color,1,0,0.1);

%Frame
hobj_pos=[[pos+[r_quad;r_quad;0]] pos+[-r_quad;-r_quad;0] pos pos+[r_quad;-r_quad;0] pos+[-r_quad;r_quad;0]];
hobj=drawLine(parent,hobj_pos,color);

%Coordinate frame
hframe = drawFrame(parent,pos,frame_len);

switch nargin
    case 4
        hquad = [hobj rot1 rot2 rot3 rot4 hframe];
    case 5
        htitle=text(pos(1)-0.1,pos(2)-0.1,pos(3)-0.1,name,'color',color,'FontSize',10);
        hquad = [hobj rot1 rot2 rot3 rot4 hframe htitle];
end

for ii=1:length(hquad)
    set(hquad(ii),'Visible',visible);
end
hold on
return