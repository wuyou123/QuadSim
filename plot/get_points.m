% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [points] = get_points(hobj)
%   _____________________
% 
%   Return the points contained in the handler for plot purposes.
%
%   [points] = get_points(hobj)
%
%   Inputs:
%       - hobj:         Object handler.
% 
%   Ouputs:
%       - points:       Handler points.
%   _____________________

x0 = get(hobj,'XData')';
y0 = get(hobj,'YData')';
z0 = get(hobj,'ZData')';

x=[];
y=[];
z=[];

for ii=1:size(x0,1)
    x = [x x0(ii,:)];
end

for ii=1:size(y0,1)
    y = [y y0(ii,:)];
end
    
for ii=1:size(z0,1)
    z = [z z0(ii,:)];
end

points=[x;y;z];

return