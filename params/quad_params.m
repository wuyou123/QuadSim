% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% QUAD_PARAMS
%
% Dynamic parameters for a quadrotor.
% QUAD_PARAMS is a script creates the workspace variable params.quad which
% describes the dynamic characterstics of a quadrotor flying robot.
%
% Properties::
%
% This is a structure with the following elements:
%
% nrotors   Number of rotors (1x1)
% J         Flyer rotational inertia matrix (3x3)
% h         Height of rotors above CoG (1x1)
% d         Length of flyer arms (1x1)
% nb        Number of blades per rotor (1x1)
% r         Rotor radius (1x1)
% c         Blade chord (1x1)
% e         Flapping hinge offset (1x1)
% Mb        Rotor blade mass (1x1)
% Mc        Estimated hub clamp mass (1x1)
% ec        Blade root clamp displacement (1x1)
% Ib        Rotor blade rotational inertia (1x1)
% Ic        Estimated root clamp inertia (1x1)
% mb        Static blade moment (1x1)
% Ir        Total rotor inertia (1x1)
% Ct        Non-dim. thrust coefficient (1x1)
% Cq        Non-dim. torque coefficient (1x1)
% sigma     Rotor solidity ratio (1x1)
% thetat    Blade tip angle (1x1)
% theta0    Blade root angle (1x1)
% theta1    Blade twist angle (1x1)
% theta75   3/4 blade angle (1x1)
% thetai    Blade ideal root approximation (1x1)
% a         Lift slope gradient (1x1)
% A         Rotor disc area (1x1)
% gamma     Lock number (1x1)
%
%
% Notes::
% - SI units are used.
%
% References::
% - Design, Construction and Control of a Large Quadrotor micro air vehicle.
%   P.Pounds, PhD thesis, 
%   Australian National University, 2007.
%   http://www.eng.yale.edu/pep5/P_Pounds_Thesis_2008.pdf
% - This is a heavy lift quadrotor
%
% See also quadrotor.

% MODEL: quadrotor

% Copyright (C) 1993-2015, by Peter I. Corke
%
% This file is part of The Robotics Toolbox for MATLAB (RTB).
% 
% RTB is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% RTB is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License
% along with RTB.  If not, see <http://www.gnu.org/licenses/>.
%
% http://www.petercorke.com

params.quad.pos_ini = [0 0 0];     %   Quadrotor initial position                  3x1

params.quad.nrotors = 4;                %   4 rotors
params.quad.g = 9.81;                   %   g       Gravity                             1x1
params.quad.rho = 1.184;                %   rho     Density of air                      1x1
params.quad.muv = 1.5e-5;               %   muv     Viscosity of air                    1x1

% Airframe
params.quad.M = 4;                      %   M       Mass                                1x1
params.quad.I.xx = 0.082;
params.quad.I.yy = 0.082;
params.quad.I.zz = 0.149;%0.160;
params.quad.J = diag([params.quad.I.xx params.quad.I.yy params.quad.I.zz]);    %   I       Flyer rotational inertia matrix     3x3

params.quad.h = -0.007;                 %   h       Height of rotors above CoG          1x1
params.quad.d = 0.315;                  %   d       Length of flyer arms                1x1

%Rotor
params.quad.nb = 2;                      %   b       Number of blades per rotor          1x1
params.quad.r = 0.165;                  %   r       Rotor radius                        1x1

params.quad.c = 0.018;                  %   c       Blade chord                         1x1

params.quad.e = 0.0;                    %   e       Flapping hinge offset               1x1
params.quad.Mb = 0.005;                 %   Mb      Rotor blade mass                    1x1
params.quad.Mc = 0.010;                 %   Mc      Estimated hub clamp mass            1x1
params.quad.ec = 0.004;                 %   ec      Blade root clamp displacement       1x1
params.quad.Ib = params.quad.Mb*(params.quad.r-params.quad.ec)^2/4 ;        %   Ib      Rotor blade rotational inertia      1x1
params.quad.Ic = params.quad.Mc*(params.quad.ec)^2/4;           %   Ic      Estimated root clamp inertia        1x1
params.quad.mb = params.quad.g*(params.quad.Mc*params.quad.ec/2+params.quad.Mb*params.quad.r/2);    %   mb      Static blade moment                 1x1
params.quad.Ir = params.quad.nb*(params.quad.Ib+params.quad.Ic);             %   Ir      Total rotor inertia                 1x1

params.quad.Ct = 0.0048;                %   Ct      Non-dim. thrust coefficient         1x1
params.quad.Cq = params.quad.Ct*sqrt(params.quad.Ct/2);         %   Cq      Non-dim. torque coefficient         1x1

params.quad.sigma = params.quad.c*params.quad.nb/(pi*params.quad.r);         %   sigma   Rotor solidity ratio                1x1
params.quad.thetat = 6.8*(pi/180);      %   thetat  Blade tip angle                     1x1
params.quad.theta0 = 14.6*(pi/180);     %   theta0  Blade root angle                    1x1
params.quad.theta1 = params.quad.thetat - params.quad.theta0;   %   theta1  Blade twist angle                   1x1
params.quad.theta75 = params.quad.theta0 + 0.75*params.quad.theta1;%   theta76 3/4 blade angle                     1x1
params.quad.thetai = params.quad.thetat*(params.quad.r/params.quad.e);      %   thetai  Blade ideal root approximation      1x1
params.quad.a = 5.5;                    %   a       Lift slope gradient                 1x1

% derived constants
params.quad.A = pi*params.quad.r^2;                 %   A       Rotor disc area                     1x1
params.quad.gamma = params.quad.rho*params.quad.a*params.quad.c*params.quad.r^4/(params.quad.Ib+params.quad.Ic);%   gamma   Lock number                         1x1

params.quad.b = params.quad.Ct*params.quad.rho*params.quad.A*params.quad.r^2; % T = b w^2
params.quad.k = params.quad.Cq*params.quad.rho*params.quad.A*params.quad.r^3; % Q = k w^2

params.quad.verbose = false;

