% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% RANGE_PARAMS
%
% Script that defines all RANGE parameters required y RANGE simulink block. 
% It increments the variables params.
% The RANGE frame can be set by its extrinsic parameters. 
% Initially (all 0's) it is considered mounted below the quadrotor and facing downwards

% Noise std_dev 
params.range.std.r = 1e-4; % Range measurement std dev

% Extrinsic parameters
params.range.pose = [0.0 0.0 0.0 0.0 0.0 0.0]'; % [x,y,z,yaw,pitch,roll]

% Range sensor model 
params.range.model = 1; % (0: Sonar; 1: Time-of-flight)

% Frequency
params.range.freq = 100; % Hz