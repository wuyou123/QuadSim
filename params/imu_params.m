% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% IMU_PARAMS 
% 
% Parameters for an IMU.
% Script that defines all IMU parameters required by the IMU simulink block. 
% It increments the variable params.
% The IMU frame is considered aligned with quadrotor frame

% ACCELEROMETERS
params.imu.std.a = 1e-5*ones(3,1); % Acc. std dev
params.imu.std.ab = 0e-3*ones(3,1); % Bias random walk std dev.

% GYROSCOPES
params.imu.std.w = 1e-5*ones(3,1); % Gyroscopes std dev.
params.imu.std.wb = 0e-3*ones(3,1); % Bias random walk std dev.

% Frequency
params.imu.freq = 100; % Hz