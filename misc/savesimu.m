% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% Save Simulation 
% 
% This function is used to ask the user if variables must be stored in
% disk. If they must, the function saves the variables in the specified
% path.


function [] = savesimu()

% Remove first 10 measures (IMU damping)
evalin('base','odom_gt = odom_gt(10:end,:);');
evalin('base','imu = imu(10:end,:);');
evalin('base','px4flow = px4flow(10:end,:);');
evalin('base','range = range(10:end,:);');

vars = {'params','odom_gt','imu','px4flow','range'};
wsvars = evalin('base','who');

go_on = false;
answered = false;
while (~answered)
    prompt = 'Store resulting variables in disk? (y/N): ';
    s = input(prompt,'s');
    if strcmp(s,'n') || isempty(s) || strcmp(s,'N')
        answered = true;
    elseif strcmp(s,'y')
        go_on = true;
        answered = true;    
    else
        disp('Invalid Option.');    
    end
end

if go_on  
    prompt = 'Please, enter relative path (default: ../QuadOdom/data/simu):';
    s = input(prompt,'s');
    if isempty(s)
        path = '../QuadOdom/data/simu';
    else
        path = s;
    end
end

if go_on 
    if ~exist(path,'dir')
        go_on = false;
        answered = false;
        while (~answered)
            prompt = 'The specified directory does not exist. Do you want to create it? (Y/n)';
            s = input(prompt,'s');
            if strcmp(s,'y') || isempty(s) || strcmp(s,'Y')
                mkdir(path);
                go_on = true;
                answered = true;
            elseif strcmp(s,'n') 
                answered = true;    
            else
                disp('Invalid Option.');    
            end  
        end            
    end
end


if go_on     
    fprintf('...Saving variables to: %s ...\n',path);
    for ii=1:length(wsvars)
        name = wsvars{ii};
        for jj=1:length(vars)
            if strcmp(name,vars{jj})
                fullpath = strcat(path,'/',name,'.mat');    
                evalin('base', sprintf('save(''%s'', ''%s'')', fullpath, name));             
            end
        end
    end
end
   
return