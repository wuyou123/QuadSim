% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   QUADROTOR SIMULATION
% 
%   This is the main script to run the simulink model: Quadrotor.mdl. 
%   The parameters of platform dynamics, simulation, sensors, etc. are
%   specified in files inside the folder: PARAMS.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

disp('MATLAB SIMULINK Quadrotor simulator')

clc
clear
close all

addpath(genpath(strcat(pwd,'/misc')));
addpath(genpath(strcat(pwd,'/params')));
addpath(genpath(strcat(pwd,'/plot')));
addpath(genpath(strcat(pwd,'/simulink')));

%% Adding necessary paths to MATLAB path
path = fileparts(mfilename('fullpath'));
all_path = strsplit(genpath(path),':');

for ii=1:size(all_path,2)-1 
    hidden = strfind(all_path(ii),'/.');
    if isempty(hidden{1})
        addpath(all_path{ii});
    end  
end

%% SIMULINK MODEL 
sim('quadrotor.mdl');
