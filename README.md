# Matlab Quadrotor Simulation

A custom MATLAB quadrotor simulation using SIMULINK. 

Citations or acknowledgements to the author are welcomed. 

## Example of usage

  Run: `main.m`
  
#### Parameters:

  All parameters are stored in `simulink/params` folder.

  Change `mission_params.m` to define the actions to be executed during the mission (e.g. take off, land, waypoints). 

  **NOTE**: The simulation time should be manually set to the ETA or larger.

  When mission actions are complete, the simulation is automatically stopped and variables stored in the workspace (e.g. Ground Truth odometry, IMU data). 

## Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)